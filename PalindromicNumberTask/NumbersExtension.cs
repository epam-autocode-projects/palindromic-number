﻿using System;

namespace PalindromicNumberTask
{
    /// <summary>
    /// Provides static method for working with integers.
    /// </summary>
    public static class NumbersExtension
    {
        /// <summary>
        /// Determines if a number is a palindromic number, see https://en.wikipedia.org/wiki/Palindromic_number.
        /// </summary>
        /// <param name="number">Verified number.</param>
        /// <returns>true if the verified number is palindromic number; otherwise, false.</returns>
        /// <exception cref="ArgumentException"> Thrown when source number is less than zero. </exception>
        public static bool IsPalindromicNumber(int number)
        {
            if (number < 0)
            {
                throw new ArgumentException("bad one");
            }

            int temp = IsPN(number, 0);

            if (temp == number)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static int IsPN(int number, int temp)
        {
            if (number == 0)
            {
                return temp;
            }

            temp = (temp * 10) + (number % 10);

            return IsPN(number / 10, temp);
        }
    }
}
